**/.terraform/*     # запретит папку .terraform и все что в ней
*.tfstate           # запретит файлы типа .tfstate
*.tfstate.*         # .tfstate. с любым расширением
crash.log           # запретит файл crash.log
*.tfvars            # запретит файлы расширения .tfvars
override.tf         # запретит файл override.tf
override.tf.json    # запретит файл override.tf.json
*_override.tf       # запретит файлы (имя)_override.tf
*_override.tf.json  # запретит файлы (имя)_override.tf.json
.terraformrc        # запретит файлы расширения .terraformrc
terraform.rc        # запретит файл terraform.rc 

new word
